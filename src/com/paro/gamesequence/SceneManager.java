package com.paro.gamesequence;

import com.paro.gamesequence.scenes.DeathScene;
import com.paro.gamesequence.scenes.IntroScene;
import com.paro.gamesequence.scenes.PlayScene;
import com.paro.gamesequence.scenes.Scene;

// TODO: Auto-generated Javadoc
/**
 * The Class SceneManager.
 */
public enum SceneManager
{
        INSTANCE;
        public enum Scenes
        {
                INTRO, PLAY, PAUSE, DEATH;
        }
        
        /** The scene number. */
        public Scenes sceneState;
        
        private Scene scene;
        
        SceneManager()
        {
                this.changeScene( Scenes.INTRO );
        }
        
        public void update()
        {
                this.scene.update();
        }
        
        public void render( GameSequence graphics )
        {
                this.scene.render( graphics );
        }
        
        public void changeScene( Scenes sceneType )
        {
                this.sceneState = sceneType;
                switch( this.sceneState )
                {
                        case DEATH :
                                this.scene = new DeathScene();
                                break;
                        case INTRO :
                                this.scene = new IntroScene();
                                break;
                        case PAUSE :
                                break;
                        case PLAY :
                                this.scene = new PlayScene();
                                break;
                        default :
                                break;
                
                }
        }
        
        public void mousePressed( int mouseX )
        {
                this.scene.mousePressed( mouseX );
        }
        
        public void mouseDragged( int mouseX )
        {
                this.scene.mouseDragged( mouseX );
        }
        
        public void tiltLeft( double val )
        {
                this.scene.tiltLeft( val );
        }
        
        public void tiltRight( double val )
        {
                this.scene.tiltRight( val );
        }
        
        public void noTilt()
        {
                this.scene.noTilt();
        }
        
        public void mouseReleased()
        {
                this.scene.mouseReleased();
        }
        
        public void pause()
        {
                this.scene.pause();
        }
        
        public void resume()
        {
                this.scene.resume();
        }
}
