package com.paro.gamesequence.scenes;

import java.util.Random;

import processing.core.PConstants;

import com.paro.gamesequence.GameSequence;
import com.paro.gamesequence.SceneManager;
import com.paro.gamesequence.SceneManager.Scenes;
import com.paro.gamesequence.Vals;
import com.paro.gamesequence.collections.EnemyCollection;
import com.paro.gamesequence.collections.PowerCollection;
import com.paro.gamesequence.physicals.Player;
import com.paro.gamesequence.physicals.Power;
import com.paro.gamesequence.physicals.enemies.Enemy;

public class PlayScene extends Scene
{
        public static Random    rand         = new Random();
        private long            currentTime;
        private long            previousTime = System.nanoTime();
        private long            delta;
        
        private double          interval     = 1 / 120.0;
        private double          acc;
        private Player          player;
        private EnemyCollection enemyCollection;
        private PowerCollection powerUps;
        private int             score;
        
        public PlayScene()
        {
                this.player = new Player();
                this.enemyCollection = new EnemyCollection( 200 );
                this.powerUps = new PowerCollection( 1000 );
        }
        
        @Override
        public void update()
        {
                this.currentTime = System.nanoTime();
                this.delta = this.currentTime - this.previousTime;
                this.previousTime = this.currentTime;
                this.acc += this.delta / 1000000000.0;
                while( this.acc >= this.interval )
                {
                        this.score++;
                        this.acc -= this.interval;
                        
                        this.powerUps.update( this.interval );
                        this.enemyCollection.update( this.interval );
                        this.player.update( this.interval );
                        this.scoreCheck();
                        this.powerCheck( this.player );
                        if( !this.player.isKillable() )
                        {
                                this.player.invincibility--;
                                if( this.player.invincibility <= 0 )
                                {
                                        this.player.killable = true;
                                }
                        }
                        this.loseCheck();
                }
        }
        
        @Override
        public void render( GameSequence graphics )
        {
                graphics.textSize( 12 * Vals.ID.SCALE );
                graphics.textAlign( PConstants.LEFT, PConstants.TOP );
                graphics.text( this.score, Vals.ID.MARGIN, Vals.ID.MARGIN );
                
                this.powerUps.render( graphics );
                this.enemyCollection.render( graphics );
                this.player.render( graphics );
                
                if( !this.player.isKillable() )
                {
                        //player.playerColor = this.gameSequence.color( 0, 255, 255 );
                        graphics.fill( 0, 0, 255 );
                        graphics.textSize( 12 * Vals.ID.SCALE );
                        graphics.textAlign( PConstants.LEFT, PConstants.TOP );
                        graphics.text( "Invincible for : " + this.player.invincibility, Vals.ID.SCALE * 100, Vals.ID.MARGIN );
                }
                else
                {
                        //player.playerColor = this.gameSequence.color( 255, 204, 0 );
                }
                
        }
        
        /**
         * Checks the score and determines rate of enemy spawning.
         */
        public void scoreCheck()
        {
                if( this.score == 1000 )
                {
                        this.enemyCollection.setRate( 150 );
                }
                else if( this.score == 2000 )
                {
                        this.enemyCollection.setRate( 100 );
                }
        }
        
        /**
         * Checks collision of player to powers.
         * 
         * @param player
         *                the player
         */
        public void powerCheck( Player player )
        {
                for( int i = 0; i < this.powerUps.getSize(); i++ )
                {
                        Power s = this.powerUps.getPower( i );
                        if( s.collisionCheck( player ) )
                        {
                                player.invincibility += this.generateInvinc();
                                player.killable = false;
                                this.powerUps.removePower( i );
                                i--;
                        }
                }
        }
        
        /**
         * Generate invincibility.
         * 
         * @return the int
         */
        public int generateInvinc()
        {
                return new Random().nextInt( 300 ) + 300;
        }
        
        /**
         * Check if player has lost due to colliding to enemies. Also performs
         * functionality of decreasing invincibility if the player is
         * invincible.
         */
        public void loseCheck()
        {
                for( int i = 0; i < this.enemyCollection.getSize(); i++ )
                {
                        Enemy e = this.enemyCollection.getEnemy( i );
                        if( e.collisionCheck( this.player ) )
                        {
                                this.enemyCollection.removeEnemy( i );
                                i--;
                                if( !this.player.killable )
                                {
                                        this.player.invincibility = this.player.invincibility - 100;
                                }
                                else
                                {
                                        this.gameOver();
                                }
                        }
                }
        }
        
        /**
         * Mouse pressed event.
         * 
         * @param mouseX
         *                the mouse x
         */
        @Override
        public void mousePressed( int mouseX )
        {
                if( mouseX > this.player.getX() )
                {
                        this.player.accelerateRight( ( mouseX - this.player.getX() ) / Vals.ID.WIDTH );
                }
                else if( mouseX < this.player.getX() )
                {
                        this.player.accelerateLeft( ( this.player.getX() - mouseX ) / Vals.ID.WIDTH );
                }
                else
                {
                        this.player.stopAccelerating();
                }
        }
        
        /**
         * Mouse dragged event.
         * 
         * @param mouseX
         *                the mouse x
         */
        @Override
        public void mouseDragged( int mouseX )
        {
                if( mouseX > this.player.getX() )
                {
                        this.player.accelerateRight( ( mouseX - this.player.getX() ) / Vals.ID.WIDTH );
                }
                else if( mouseX < this.player.getX() )
                {
                        this.player.accelerateLeft( ( this.player.getX() - mouseX ) / Vals.ID.WIDTH );
                }
                else
                {
                        this.player.stopAccelerating();
                }
        }
        
        /**
         * Tilt left.
         */
        @Override
        public void tiltLeft( double val )
        {
                this.player.accelerateLeft( val );
        }
        
        /**
         * Tilt right.
         */
        @Override
        public void tiltRight( double val )
        {
                this.player.accelerateRight( val );
        }
        
        /**
         * No tilt.
         */
        @Override
        public void noTilt()
        {
                this.player.stopAccelerating();
        }
        
        @Override
        public void mouseReleased()
        {
                this.player.stopAccelerating();
        }
        
        public void gameOver()
        {
                Vals.ID.currentScore = this.score;
                SceneManager.INSTANCE.changeScene( Scenes.DEATH );
        }
        
        @Override
        public void pause()
        {
        }
        
        @Override
        public void resume()
        {
        }
}
