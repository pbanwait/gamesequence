package com.paro.gamesequence.scenes;

import com.paro.gamesequence.GameSequence;

public abstract class Scene
{
        public abstract void update();
        
        public abstract void render( GameSequence graphics );
        
        public void mousePressed( int mouseX )
        {
        }
        
        public void mouseDragged( int mouseX )
        {
        }
        
        public void tiltLeft( double val )
        {
        }
        
        public void tiltRight( double val )
        {
        }
        
        public void noTilt()
        {
        }
        
        public void mouseReleased()
        {
        }
        
        public void pause()
        {
        }
        
        public void resume()
        {
        }
}
