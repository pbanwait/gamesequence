package com.paro.gamesequence.scenes;

import processing.core.PConstants;

import com.paro.gamesequence.GameSequence;
import com.paro.gamesequence.SceneManager;
import com.paro.gamesequence.SceneManager.Scenes;
import com.paro.gamesequence.Vals;

public class DeathScene extends Scene
{
        
        @Override
        public void render( GameSequence graphics )
        {
                graphics.fill( 255, 0, 0 );
                graphics.textSize( 18 * Vals.ID.SCALE );
                graphics.textAlign( PConstants.CENTER, PConstants.CENTER );
                graphics.text( "You Lose!!\nYour Score: " + Vals.ID.currentScore + "\nYour Best Score : " + Vals.ID.bestScore,
                        Vals.ID.WIDTH / 2, Vals.ID.HEIGHT / 2 );
        }
        
        @Override
        public void update()
        {
                if( Vals.ID.currentScore > Vals.ID.bestScore )
                {
                        Vals.ID.bestScore = Vals.ID.currentScore;
                }
        }
        
        @Override
        public void mousePressed( int mouseX )
        {
                SceneManager.INSTANCE.changeScene( Scenes.PLAY );
        }
}
