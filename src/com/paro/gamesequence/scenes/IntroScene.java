package com.paro.gamesequence.scenes;

import processing.core.PConstants;

import com.paro.gamesequence.GameSequence;
import com.paro.gamesequence.SceneManager;
import com.paro.gamesequence.SceneManager.Scenes;
import com.paro.gamesequence.Vals;

public class IntroScene extends Scene
{
        @Override
        public void update()
        {
                
        }
        
        @Override
        public void render( GameSequence graphics )
        {
                graphics.fill( 255, 255, 255 );
                graphics.textSize( 18 * Vals.ID.SCALE );
                graphics.textAlign( PConstants.CENTER, PConstants.CENTER );
                graphics.text( "Collision Game\nClick to Begin Game", Vals.ID.WIDTH / 2, Vals.ID.HEIGHT / 2 );
        }
        
        @Override
        public void mousePressed( int mouseX )
        {
                SceneManager.INSTANCE.changeScene( Scenes.PLAY );
        }
}
