package com.paro.gamesequence.collections;

import java.util.ArrayList;

import com.paro.gamesequence.GameSequence;
import com.paro.gamesequence.physicals.Power;
import com.paro.gamesequence.scenes.PlayScene;

// TODO: Auto-generated Javadoc
/**
 * The Class PowerCollection.
 */
public class PowerCollection
{
        
        /** The rate at which powers are generated. */
        private int                rate;
        
        /** The powers list. */
        private ArrayList< Power > powers;
        
        /**
         * Instantiates a new power collection.
         * 
         * @param srate
         *                the srate
         */
        public PowerCollection( int srate )
        {
                this.rate = srate;
                this.powers = new ArrayList< Power >();
        }
        
        /**
         * Gets the power.
         * 
         * @param index
         *                the index of the power to get
         * @return the powers
         */
        public Power getPower( int index )
        {
                return this.powers.get( index );
        }
        
        /**
         * Removes the power.
         * 
         * @param index
         *                the powers index to remove
         */
        public void removePower( int index )
        {
                this.powers.remove( index );
        }
        
        /**
         * Updates power graphics.
         * 
         * @param graphics
         *                the graphics
         */
        public void render( GameSequence graphics )
        {
                for( int i = 0; i < this.powers.size(); i++ )
                {
                        Power s = this.powers.get( i );
                        s.render( graphics );
                }
        }
        
        /**
         * Gets the size of the list.
         * 
         * @return the size
         */
        public int getSize()
        {
                return this.powers.size();
        }
        
        public void update( double interval )
        {
                if( PlayScene.rand.nextInt( this.rate ) == 0 )
                {
                        this.powers.add( new Power() );
                }
                for( int i = 0; i < this.powers.size(); i++ )
                {
                        Power s = this.powers.get( i );
                        s.update( interval );
                        if( s.dead() )
                        {
                                this.powers.remove( i );
                                i--;
                        }
                }
        }
}
