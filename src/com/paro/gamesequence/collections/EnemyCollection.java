package com.paro.gamesequence.collections;

import java.util.ArrayList;

import processing.core.PConstants;

import com.paro.gamesequence.GameSequence;
import com.paro.gamesequence.Vals;
import com.paro.gamesequence.physicals.enemies.Enemy;
import com.paro.gamesequence.physicals.enemies.SpecialEnemy;
import com.paro.gamesequence.scenes.PlayScene;

// TODO: Auto-generated Javadoc
/**
 * The Class EnemyCollection.
 */
public class EnemyCollection
{
        
        /** The rate at which enemies are created. */
        private int                rate;
        
        /** The enemies list. */
        private ArrayList< Enemy > enemies;
        
        /**
         * Instantiates a new enemy collection.
         * 
         * @param srate
         *                the srate
         */
        public EnemyCollection( int srate )
        {
                this.rate = srate;
                this.enemies = new ArrayList< Enemy >();
        }
        
        /**
         * Update all enemy graphics.
         * 
         * @param graphics
         *                the graphics
         */
        public void render( GameSequence graphics )
        {
                graphics.fill( 255, 0, 255 );
                graphics.textAlign( PConstants.LEFT, PConstants.TOP );
                graphics.text( "Currently " + this.enemies.size() + " enemies", Vals.ID.SCALE * 300, Vals.ID.MARGIN );
                for( int i = 0; i < this.enemies.size(); i++ )
                {
                        Enemy e = this.enemies.get( i );
                        e.render( graphics );
                }
        }
        
        /**
         * Gets the size of the list.
         * 
         * @return the number of enemies
         */
        public int getSize()
        {
                return this.enemies.size();
        }
        
        /**
         * Gets the enemy.
         * 
         * @param index
         *                the index of the enemy to retrieve
         * @return the enemy
         */
        public Enemy getEnemy( int index )
        {
                return this.enemies.get( index );
        }
        
        /**
         * Removes the enemy.
         * 
         * @param index
         *                the index of the enemy to remove
         */
        public void removeEnemy( int index )
        {
                this.enemies.remove( index );
        }
        
        /**
         * Sets the rate.
         * 
         * @param srate
         *                sets the rate of the enemy generator
         */
        public void setRate( int srate )
        {
                this.rate = srate;
        }
        
        public void update( double interval )
        {
                if( PlayScene.rand.nextInt( this.rate ) == 0 )
                {
                        this.enemies.add( new Enemy() );
                }
                else if( PlayScene.rand.nextInt( this.rate ) * 2 == 0 )
                {
                        this.enemies.add( new SpecialEnemy() );
                }
                for( int i = 0; i < this.enemies.size(); i++ )
                {
                        Enemy e = this.enemies.get( i );
                        e.update( interval );
                        if( e.dead() )
                        {
                                this.enemies.remove( i );
                                i--;
                        }
                }
        }
}
