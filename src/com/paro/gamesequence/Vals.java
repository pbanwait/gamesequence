package com.paro.gamesequence;

import processing.core.PImage;

// TODO: Auto-generated Javadoc
/**
 * The Class Resources.
 */
public enum Vals
{
        ID;
        
        /** The normal player image. */
        public PImage normalPlayerImage;
        
        /** The invincible player image. */
        public PImage invinciblePlayerImage;
        
        /** The enemy image. */
        public PImage enemyImage;
        
        /** The special enemy image. */
        public PImage specialImage;
        
        /** The power image. */
        public PImage powerImage;
        
        /** The margin. */
        public float  MARGIN;
        
        /** The view height. */
        public int    HEIGHT;
        
        /** The view width. */
        public int    WIDTH;
        
        public float  SCALE;
        
        public int    bestScore;
        
        public int    currentScore;
        
        public void destroy()
        {
                this.normalPlayerImage = null;
                this.invinciblePlayerImage = null;
                this.enemyImage = null;
                this.specialImage = null;
                this.powerImage = null;
        }
}
