package com.paro.gamesequence.physicals;

import processing.core.PConstants;
import processing.core.PImage;
import processing.core.PVector;

import com.paro.gamesequence.GameSequence;
import com.paro.gamesequence.Vals;

// TODO: Auto-generated Javadoc
/**
 * The Class Player.
 */
public class Player
{
        
        /** The position. */
        PVector             position;
        
        /** The velocity. */
        PVector             velocity;
        
        /** The acceleration. */
        PVector             acceleration;
        
        /** The normal image. */
        PImage              normalImage;
        
        /** The invincible image. */
        PImage              invincibleImage;
        
        /** The size. */
        public static float size;
        
        /** The max velocity. */
        float               maxV;
        
        /** The max acceleration. */
        float               maxA;
        
        /** Determines player state. */
        public boolean      killable      = true;
        
        /** The amount of invincibility the player has. */
        public int          invincibility = 0;
        
        /**
         * Instantiates a new player.
         */
        public Player()
        {
                size = Vals.ID.SCALE * 30;
                this.position = new PVector( Vals.ID.MARGIN, Vals.ID.HEIGHT - Vals.ID.MARGIN - size );
                this.velocity = new PVector( 0, 0 );
                this.acceleration = new PVector( 0, 0 );
                this.maxV = 300.0f * Vals.ID.SCALE;
                this.maxA = 200.0f * Vals.ID.SCALE;
        }
        
        /**
         * Accelerate left.
         * 
         * @param val
         */
        public void accelerateLeft( double val )
        {
                if( this.velocity.x > 0 ) // quicker acceleration when trying to change directions
                {
                        this.acceleration.set( (float) ( -this.maxA * 2 * val ), 0, 0 );
                }
                else
                {
                        this.acceleration.set( (float) ( -this.maxA * val ), 0, 0 );
                }
        }
        
        /**
         * Accelerate right.
         * 
         * @param val
         */
        public void accelerateRight( double val )
        {
                if( this.velocity.x < 0 ) // quicker acceleration when trying to change directions
                {
                        this.acceleration.set( (float) ( this.maxA * 2 * val ), 0, 0 );
                }
                else
                {
                        this.acceleration.set( (float) ( this.maxA * val ), 0, 0 );
                }
        }
        
        /**
         * Reset velocity.
         */
        public void resetVelocity()
        {
                this.velocity.x = 0;
        }
        
        /**
         * Stop accelerating.
         */
        public void stopAccelerating()
        {
                this.acceleration.set( 0, 0, 0 );
        }
        
        /**
         * Update player graphics.
         * 
         * @param graphics
         *                the graphics
         */
        public void render( GameSequence graphics )
        {
                graphics.fill( 0, 255, 0 );
                graphics.textSize( 12 * Vals.ID.SCALE );
                graphics.textAlign( PConstants.RIGHT, PConstants.TOP );
                graphics.text( "Speed : " + this.velocity, Vals.ID.WIDTH - Vals.ID.MARGIN, Vals.ID.MARGIN );
                //graphics.fill( playerColor );
                //graphics.noStroke(); // Don't draw a stroke around shapes
                if( this.killable )
                {
                        graphics.image( Vals.ID.normalPlayerImage, this.position.x, this.position.y, size, size );
                }
                else
                {
                        graphics.image( Vals.ID.invinciblePlayerImage, this.position.x, this.position.y, size, size );
                }
                //position.x = mouseX;
                //pg.rect(mouseX, position.y, size, size);
        }
        
        /**
         * Checks if player is killable.
         * 
         * @return true, if is killable
         */
        public boolean isKillable()
        {
                return this.killable;
        }
        
        /**
         * Gets the x.
         * 
         * @return the x
         */
        public float getX()
        {
                return this.position.x;
        }
        
        /**
         * Gets the y.
         * 
         * @return the y
         */
        public float getY()
        {
                return this.position.y;
        }
        
        public void update( double interval )
        {
                this.velocity.add( PVector.mult( this.acceleration, (float) interval ) );
                if( this.velocity.x < -this.maxV )
                {
                        this.velocity.x = -this.maxV;
                }
                else if( this.velocity.x > this.maxV )
                {
                        this.velocity.x = this.maxV;
                }
                this.position.add( PVector.mult( this.velocity, (float) interval ) );
                if( this.position.x >= Vals.ID.WIDTH - ( Vals.ID.MARGIN + size ) )
                {
                        this.resetVelocity();
                        this.position.x = Vals.ID.WIDTH - ( Vals.ID.MARGIN + size );
                }
                else if( this.position.x <= Vals.ID.MARGIN )
                {
                        this.resetVelocity();
                        this.position.x = Vals.ID.MARGIN;
                }
        }
}
