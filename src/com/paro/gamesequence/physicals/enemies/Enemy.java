package com.paro.gamesequence.physicals.enemies;

import processing.core.PVector;

import com.paro.gamesequence.GameSequence;
import com.paro.gamesequence.Vals;
import com.paro.gamesequence.physicals.Player;
import com.paro.gamesequence.scenes.PlayScene;

// TODO: Auto-generated Javadoc
/**
 * The Class Enemy.
 */
public class Enemy
{
        /** The position. */
        protected PVector position;
        
        /** The velocity. */
        protected PVector velocity;
        
        /** The acceleration. */
        protected PVector acceleration;
        
        protected float   size;
        
        /**
         * Instantiates a new enemy.
         */
        public Enemy()
        {
                this.size = Vals.ID.SCALE * 8;
                this.acceleration = new PVector( 0, 50 * Vals.ID.SCALE );
                this.velocity =
                        new PVector( Vals.ID.SCALE * 100 * ( PlayScene.rand.nextFloat() * 2 - 1 ), Vals.ID.SCALE * 30
                                * ( PlayScene.rand.nextFloat() * 2 - 1 ) );
                this.position = new PVector( Vals.ID.WIDTH * PlayScene.rand.nextFloat(), 0 );
        }
        
        /**
         * Instantiates a new enemy with randomized parameter.
         * 
         * @param pos
         *                the pos
         * @param randVal
         *                the random value
         */
        Enemy( PVector pos )
        {
                this();
                this.acceleration.set( 0, 75 * Vals.ID.SCALE, 0 );
                this.position.set( pos );
        }
        
        /**
         * Update the display of the enemy.
         * 
         * @param graphics
         *                the graphics
         */
        public void render( GameSequence graphics )
        {
                //graphics.fill( 255, 0, 0 );
                //graphics.noStroke(); // Don't draw a stroke around shapes
                graphics.image( Vals.ID.enemyImage, this.position.x, this.position.y, this.size, this.size );
        }
        
        /**
         * Check if the enemy is out of bounds on screen.
         * 
         * @return true, state of enemy
         */
        public boolean dead()
        {
                if( this.position.y >= Vals.ID.HEIGHT - Vals.ID.MARGIN || this.position.x < 0 || this.position.x > Vals.ID.WIDTH )
                {
                        return true;
                }
                return false;
        }
        
        /**
         * Collision check.
         * 
         * @param player
         *                the player
         * @return true, if collided
         */
        public boolean collisionCheck( Player player )
        {
                if( this.position.y >= Vals.ID.HEIGHT - Vals.ID.MARGIN - Player.size )
                {
                        if( this.position.x >= player.getX() && this.position.x <= player.getX() + Player.size )
                        {
                                return true;
                        }
                }
                return false;
        }
        
        public void update( double interval )
        {
                this.velocity.add( PVector.mult( this.acceleration, (float) interval ) );
                this.position.add( PVector.mult( this.velocity, (float) interval ) );
        }
}
