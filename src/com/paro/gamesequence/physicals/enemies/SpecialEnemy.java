package com.paro.gamesequence.physicals.enemies;

import processing.core.PVector;

import com.paro.gamesequence.GameSequence;
import com.paro.gamesequence.Vals;
import com.paro.gamesequence.physicals.Player;
import com.paro.gamesequence.scenes.PlayScene;

// TODO: Auto-generated Javadoc
/**
 * The Class SpecialEnemy.
 */
public class SpecialEnemy extends Enemy
{
        
        /** Value determining if the special enemy has split yet or not. */
        private boolean split;
        
        /** The enemy particles array. */
        private Enemy[] particles;
        private int     arraySize = 3;
        
        /** The split location. */
        private int     splitLocation;
        
        /**
         * Instantiates a new special enemy.
         */
        public SpecialEnemy()
        {
                super();
                this.splitLocation = PlayScene.rand.nextInt( (int) ( Vals.ID.HEIGHT - ( Vals.ID.MARGIN + Player.size ) ) );
                this.split = false;
                this.velocity =
                        new PVector( Vals.ID.SCALE * 50 * ( PlayScene.rand.nextFloat() * 2 - 1 ), Vals.ID.SCALE * 15
                                * ( PlayScene.rand.nextFloat() * 2 - 1 ) );
                this.acceleration = new PVector( 0, Vals.ID.SCALE * 10 );
        }
        
        /* (non-Javadoc)
         * @see com.paro.gamesequence.physicals.enemies.Enemy#update(processing.core.PGraphics)
         */
        @Override
        public void render( GameSequence graphics )
        {
                if( !this.split )
                {
                        //graphics.fill( 50, 0, 0 ); // Use color variable 'c' as fill color
                        //graphics.noStroke(); // Don't draw a stroke around shapes
                        graphics.image( Vals.ID.specialImage, this.position.x, this.position.y, this.size, this.size );
                }
                else
                {
                        for( int i = 0; i < this.arraySize; i++ )
                        {
                                this.particles[i].render( graphics );
                        }
                }
        }
        
        /* (non-Javadoc)
         * @see com.paro.gamesequence.physicals.enemies.Enemy#dead()
         */
        @Override
        public boolean dead()
        {
                if( this.split )
                {
                        for( int i = 0; i < this.arraySize; i++ )
                        {
                                if( !this.particles[i].dead() )
                                {
                                        return false;
                                }
                        }
                        return true;
                }
                if( this.position.y >= Vals.ID.HEIGHT - Vals.ID.MARGIN || this.position.x < 0 || this.position.x > Vals.ID.WIDTH )
                {
                        return true;
                }
                return false;
        }
        
        /* (non-Javadoc)
         * @see com.paro.gamesequence.physicals.enemies.Enemy#collisionCheck(com.paro.gamesequence.physicals.Player)
         */
        @Override
        public boolean collisionCheck( Player player )
        {
                if( !this.split )
                {
                        if( this.position.y >= Vals.ID.HEIGHT - Vals.ID.MARGIN - Player.size )
                        {
                                if( this.position.x >= player.getX() && this.position.x <= player.getX() + Player.size )
                                {
                                        return true;
                                }
                                return false;
                        }
                }
                else
                {
                        for( int i = 0; i < this.arraySize; i++ )
                        {
                                if( this.particles[i].collisionCheck( player ) )
                                {
                                        return true;
                                }
                        }
                        return false;
                }
                return false;
        }
        
        @Override
        public void update( double interval )
        {
                if( !this.split )
                {
                        super.update( interval );
                        if( this.position.y >= this.splitLocation )
                        {
                                this.split = true;
                                this.particles = new Enemy[ this.arraySize ];
                                for( int i = 0; i < this.arraySize; i++ )
                                {
                                        this.particles[i] = new Enemy( this.position );
                                }
                        }
                }
                else
                {
                        for( int i = 0; i < this.arraySize; i++ )
                        {
                                this.particles[i].update( interval );
                        }
                }
        }
}
