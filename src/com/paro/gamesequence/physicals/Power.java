package com.paro.gamesequence.physicals;

import processing.core.PVector;

import com.paro.gamesequence.GameSequence;
import com.paro.gamesequence.Vals;
import com.paro.gamesequence.scenes.PlayScene;

// TODO: Auto-generated Javadoc
/**
 * The Class Power.
 */
public class Power
{
        
        /** The position. */
        public PVector position;
        
        /** The velocity. */
        PVector        velocity;
        
        /** The acceleration. */
        PVector        acceleration;
        
        private float  size;
        
        /**
         * Instantiates a new power.
         */
        public Power()
        {
                this.acceleration = new PVector( 0, Vals.ID.SCALE * 100 );
                this.velocity =
                        new PVector( Vals.ID.SCALE * 10 * ( PlayScene.rand.nextFloat() * 2 - 1 ), Vals.ID.SCALE * 10
                                * ( PlayScene.rand.nextFloat() * 2 - 1 ) );
                this.position = new PVector( Vals.ID.WIDTH * PlayScene.rand.nextFloat(), 0 );
                this.size = Vals.ID.SCALE * 8;
        }
        
        /**
         * Update power graphics.
         * 
         * @param graphics
         *                the graphics
         */
        public void render( GameSequence graphics )
        {
                //graphics.fill( 0, 0, 255 );
                //graphics.noStroke(); // Don't draw a stroke around shapes
                graphics.image( Vals.ID.powerImage, this.position.x, this.position.y, this.size, this.size );
        }
        
        /**
         * Collision check.
         * 
         * @param player
         *                the player
         * @return true, if collided
         */
        public boolean collisionCheck( Player player )
        {
                if( this.position.y >= Vals.ID.HEIGHT - Vals.ID.MARGIN - Player.size )
                {
                        if( this.position.x >= player.getX() && this.position.x <= player.getX() + Player.size )
                        {
                                return true;
                        }
                }
                return false;
        }
        
        public void update( double interval )
        {
                this.velocity.add( PVector.mult( this.acceleration, (float) interval ) );
                this.position.add( PVector.mult( this.velocity, (float) interval ) );
        }
        
        /**
         * Check if the power is out of bounds on screen.
         * 
         * @return true, state of enemy
         */
        public boolean dead()
        {
                if( this.position.y >= Vals.ID.HEIGHT - Vals.ID.MARGIN || this.position.x < 0 || this.position.x > Vals.ID.WIDTH )
                {
                        return true;
                }
                return false;
        }
}
