package com.paro.gamesequence;

import processing.core.PApplet;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.WindowManager;

// TODO: Auto-generated Javadoc
/**
 * The Class GameSequence.
 */
public class GameSequence extends PApplet implements SensorEventListener
{
        /** The scene. */
        private SceneManager  sceneManager;
        
        /** The sensor manager. */
        private SensorManager sensorManager;
        
        /** The accelerometer values. */
        private double        ay;
        
        @Override
        public void onCreate( Bundle savedInstanceState )
        {
                this.orientation( LANDSCAPE );
                this.getWindow().addFlags( WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON );
                this.sensorManager = (SensorManager) this.getSystemService( SENSOR_SERVICE );
                this.sensorManager.registerListener( this, this.sensorManager.getDefaultSensor( Sensor.TYPE_ACCELEROMETER ),
                        SensorManager.SENSOR_DELAY_NORMAL );
                this.sceneManager = SceneManager.INSTANCE;
                super.onCreate( savedInstanceState );
        }
        
        @Override
        public void pause()
        {
                if( this.sceneManager != null )
                {
                        this.sceneManager.pause();
                }
        }
        
        @Override
        public void resume()
        {
                if( this.sceneManager != null )
                {
                        this.sceneManager.resume();
                }
                
        }
        
        @Override
        public void setup()
        {
                Vals.ID.HEIGHT = this.displayHeight;
                Vals.ID.WIDTH = this.displayWidth;
                Vals.ID.SCALE = Vals.ID.WIDTH / 640.0f;
                Vals.ID.MARGIN = (int) ( 10 * Vals.ID.SCALE );
                Vals.ID.normalPlayerImage = this.loadImage( "player_normal.png" );
                Vals.ID.invinciblePlayerImage = this.loadImage( "player_invincible.png" );
                Vals.ID.enemyImage = this.loadImage( "enemy.png" );
                Vals.ID.specialImage = this.loadImage( "special.png" );
                Vals.ID.powerImage = this.loadImage( "power.png" );
        }
        
        /* (non-Javadoc)
         * @see processing.core.PApplet#draw()
         */
        @Override
        public void draw()
        {
                this.sceneManager.update();
                this.background( 0, 140 );
                this.sceneManager.render( this );
        }
        
        /* (non-Javadoc)
         * @see processing.core.PApplet#mousePressed()
         */
        @Override
        public void mousePressed()
        {
                this.sceneManager.mousePressed( this.mouseX );
        }
        
        /* (non-Javadoc)
         * @see processing.core.PApplet#mouseDragged()
         */
        @Override
        public void mouseDragged()
        {
                this.sceneManager.mouseDragged( this.mouseX );
        }
        
        @Override
        public void mouseReleased()
        {
                this.sceneManager.mouseReleased();
        }
        
        /* (non-Javadoc)
         * @see processing.core.PApplet#sketchRenderer()
         */
        @Override
        public String sketchRenderer()
        {
                return P2D;
        }
        
        @Override
        public boolean isGL()
        {
                return true;
        }
        
        /* (non-Javadoc)
         * @see android.hardware.SensorEventListener#onAccuracyChanged(android.hardware.Sensor, int)
         */
        @Override
        public void onAccuracyChanged( Sensor arg0, int arg1 )
        {
        }
        
        /* (non-Javadoc)
         * @see android.hardware.SensorEventListener#onSensorChanged(android.hardware.SensorEvent)
         */
        @Override
        public void onSensorChanged( SensorEvent arg0 )
        {
                if( arg0.sensor.getType() == Sensor.TYPE_ACCELEROMETER )
                {
                        this.ay = arg0.values[1];
                        if( this.ay < 0 )
                        {
                                this.sceneManager.tiltLeft( -this.ay );
                        }
                        else if( this.ay > 0 )
                        {
                                this.sceneManager.tiltRight( this.ay );
                        }
                        else
                        {
                                this.sceneManager.noTilt();
                        }
                }
        }
}
