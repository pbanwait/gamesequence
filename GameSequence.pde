SceneManager scene;
PGraphics pg;
Player player;

int margin;

int bestScore;
int currentScore;
color c;

void setup()
{
  //frameRate(60);
  //size(width, height, P2D);
  size(640, 480, JAVA2D);
  orientation(LANDSCAPE);
  margin = (height/480)*10;
  background(0);
  bestScore = 0;
  currentScore = 0;
  scene = new SceneManager(0);
}

void draw()
{
  fill(0, 0, 0, 40);
  rect(0, 0, width, height);
  pg = createGraphics(width, height);
  pg.beginDraw();
  switch(scene.getScene())
  {
    case 0:
      scene.introScene();
      break;
    case 1:
      scene.playScene();
      break;
    case 2:
      scene.pauseScene();
      break;
    case 3:
      scene.deathScene();
      scene = new SceneManager(3);
      break;
  }
  pg.endDraw();
  image(pg, 0, 0);
}

void debugText(String s)
{
  c = color(140, 0, 255);
  pg.fill(c);
  pg.textSize(18);
  pg.text(s, (width/640)*100, (height/480)*100);
}

void mousePressed()
{
  if(scene.getScene() == 0 || scene.getScene() == 3)
  {
    scene.setScene(1);
    background(0);
  }
  else if(scene.getScene() == 1)
  {
    if(mouseX > player.position.x)
    {
      player.moveRight();
    }
    else if(mouseX < player.position.x)
    {
      player.moveLeft();
    }
  }
}

void mouseDragged()
{
  if(scene.getScene() == 1)
  {
    if(mouseX > player.position.x)
    {
      player.moveRight();
    }
    else if(mouseX < player.position.x)
    {
      player.moveLeft();
    }
  }
}

void keyPressed()
{
  if(scene.getScene() == 1)
  {
    if(key == ' ')
    {
      player.resetVelocity();
    }
    else if(key == 'a')
    {
      player.moveLeft();
    }
    else if(key == 'd')
    {
      player.moveRight();
    }
    else if(key == CODED)
    {
      if(keyCode == LEFT)
      {
        player.moveLeft();
      }
      else if(keyCode == RIGHT)
      {
        player.moveRight();
      }
    }
  }
}

class SceneManager
{
  private int sceneNumber;
  private int score;
  private EnemyCollection enemyCollection;
  private PowerCollection powerUps;
  
  boolean killable = true;
  int invincibility = 0;
  
  SceneManager(int sceneNum)
  {
    player = new Player();
    enemyCollection = new EnemyCollection(60);
    powerUps = new PowerCollection(1000);
    sceneNumber = sceneNum;
    score = 0;
  }
  
  int getScene()
  {
    return this.sceneNumber;
  }
  void setScene(int sceneNum)
  {
    sceneNumber = sceneNum;
  }
  
  
  void introScene()
  {
    c = color(255, 255, 255);
    pg.fill(c);
    pg.textSize(18);
    pg.text("Collision Game", (width/640)*320, (height/480)*240);
    pg.text("Left Click to Begin Game", (width/640)*320, (height/480)*258);
  }
  void playScene()
  {
    
    c = color(0, 255, 255);
    pg.fill(c);
    pg.text(score++, margin, height - margin);
    
    powerUps.update();
    enemyCollection.update();
    player.update();
    
    scoreCheck();
    powerCheck();
    
    if(!killable)
    {
      player.playerColor = color(0, 255, 255);
      c = color(0, 0, 255);
      pg.fill(c);
      pg.text("Invincible for : " + invincibility, (width/640)*100, height - margin);
      invincibility--;
      if(invincibility == 0)
      {
        player.playerColor = color(255, 204, 0);
        killable = true;
      }
    }
    
    loseCheck();
  }
  void pauseScene()
  {
    
  }
  void deathScene()
  {
    c = color(255, 0, 0);
    pg.fill(c);
    if(score > bestScore)
    {
      bestScore = score;
    }
    if(score != 0)
    {
      currentScore = score;
    }
    pg.textSize(18);
    pg.text("You Lose!!\nYour Score: " + currentScore + "\nYour Best Score : " + bestScore, (width/640)*300, (height/480)*200);
  }
  
  
  
  void scoreCheck()
  {
    if(score == 1000)
    {
      enemyCollection.rate = 50;
    }
    else if(score == 2000)
    {
      enemyCollection.rate = 30;
    }
  }
  
  void powerCheck()
  {
    for(int i = 0; i < powerUps.powers.size(); i++)
    {
      Power s = powerUps.powers.get(i);
      if(s.position.y <= margin + player.size)
      {
        if(s.position.x >= player.position.x && s.position.x <= player.position.x + player.size)
        {
          invincibility += generateInvinc();
          killable = false;
          powerUps.powers.remove(i);
          i--;
        }
      }
    }
  }
  int generateInvinc()
  {
    return (int)random(400, 1000);
  }
  void loseCheck()
  {
    for(int i = 0; i < enemyCollection.enemies.size(); i++)
    {
      Enemy e = enemyCollection.enemies.get(i);
      if(e.collisionCheck())
      {
        enemyCollection.enemies.remove(i);
        i--;
        if(killable)
        {
          sceneNumber = 3;
        }
      }
    }
  }
}

class Player
{
  PVector position;
  PVector velocity;
  PVector acceleration;
  
  int size;
  float maxV;
  float maxA;
  
  color playerColor;
  Player()
  {
    position = new PVector(margin, margin);
    velocity = new PVector(0, 0);
    size = (height/480)*50;
    maxV = 9.0;
    maxA = 1.0;
    playerColor = color(255, 204, 0);
  }
  void moveLeft()
  {
    acceleration = new PVector((width/640)*-maxA, 0);
    if(velocity.x >= (width/640)*-maxV)
    {
      velocity.add(acceleration);
    }
  }
  void moveRight()
  {
    acceleration = new PVector((width/640)*maxA, 0);
    if(velocity.x <= (width/640)*maxV)
    {
      velocity.add(acceleration);
    }
  }
  void resetVelocity()
  {
    velocity.x = 0;
  }
  void update()
  {
    c = color(0, 255, 0);
    pg.fill(c);
    pg.text("Speed : " + velocity, (width/640)*500, height - margin);
    position.add(velocity);
    if(position.x >= width - (margin + size))
    {
      velocity.x = 0;
      position.x = width - (margin + size);
    }
    else if(position.x <= margin)
    {
      velocity.x = 0;
      position.x = margin;
    }
    
    pg.fill(playerColor);
    pg.noStroke();  // Don't draw a stroke around shapes
    pg.rect(position.x, position.y, size, size);
    //position.x = mouseX;
    //pg.rect(mouseX, position.y, size, size);
  }
}

class EnemyCollection
{
  int rate;
  ArrayList<Enemy> enemies;
  EnemyCollection(int srate)
  {
    rate = srate;
    enemies = new ArrayList<Enemy>();
  }
  void update()
  {
    c = color(255, 0, 255);
    pg.fill(c);
    pg.text("Currently " + enemies.size() + " enemies", (width/640)*300, height - margin);
    if ((int)random(0, rate) == 0)
    {
      enemies.add(new Enemy());
    }
    else if((int)random(0, rate*3) == 0)
    {
      enemies.add(new SpecialEnemy());
    }
    for(int i = 0; i < enemies.size(); i++)
    {
      Enemy e = enemies.get(i);
      e.update();
      if(e.dead())
      {
        enemies.remove(i);
        i--;
      }
    }
  }
}

class Enemy
{
  PVector position;
  PVector velocity;
  PVector acceleration;
  
  Enemy()
  {
    acceleration = new PVector(0,(height/480)*-0.30);
    velocity = new PVector((width/640)*random(-1,1), (height/480)*random(0, 2));
    position = new PVector((width/640)*random(100, 500), (height/480)*480);
  }
  
  Enemy(PVector pos, float randVal)
  {
    acceleration = new PVector(0,(height/480)*random(-0.30));
    velocity = new PVector((width/640)*random(-randVal,randVal), (height/480)*random(0, randVal));
    position = pos;
  }
  
  
  void update()
  {
    velocity.add(acceleration);
    position.add(velocity);
    
    c = color(255, 0, 0);  // Define color 'c'
    pg.fill(c);  // Use color variable 'c' as fill color
    pg.noStroke();  // Don't draw a stroke around shapes
    pg.ellipse(position.x, position.y, (height/480)*8, (height/480)*8);
  }
  boolean dead()
  {
    if(position.y <= margin)
    {
      return true;
    }
    return false;
  }
  boolean collisionCheck()
  {
    if(position.y <= margin + player.size)
    {
      if(position.x >= player.position.x && position.x <= player.position.x + player.size)
      {
        return true;
      }
    }
    return false;
  }
}

class SpecialEnemy extends Enemy
{
  PVector position;
  PVector velocity;
  PVector acceleration;
  
  boolean split;
  Enemy[] particles;
  int splitLocation;
  SpecialEnemy()
  {
    splitLocation = (int)random(margin + player.size, height);
    split = false;
    
    acceleration = new PVector(0,(height/480)*-0.10);
    velocity = new PVector((width/640)*random(-1,1), (height/480)*random(0, 2));
    position = new PVector((width/640)*random(100, 500), (height/480)*480);
  }

  void update()
  {
    if(!split)
    {
      velocity.add(acceleration);
      position.add(velocity);
      
      c = color(50, 0, 0);  // Define color 'c'
      pg.fill(c);  // Use color variable 'c' as fill color
      pg.noStroke();  // Don't draw a stroke around shapes
      pg.ellipse(position.x, position.y, (height/480)*8, (height/480)*8);
      if(position.y <= splitLocation)
      {
        split = true;
        particles = new Enemy[3];
        Enemy a = new Enemy(position, random(5));
        Enemy b = new Enemy(position, random(5));
        Enemy c = new Enemy(position, random(5));
        particles[0] = a;
        particles[1] = b;
        particles[2] = c;
      }
    }
    else
    {
      for(int i = 0; i < 3; i++)
      {
        particles[i].update();
      }
    }
  }
  
  boolean dead()
  {
    if(split)
    {
      if(particles[0].dead() && particles[1].dead() && particles[2].dead())
      {
        return true;
      }
      return false;
    }
    if(position.y <= margin)
    {
      return true;
    }
    return false;
  }
  
  boolean collisionCheck()
  {
    if(!split)
    {
      if(position.y <= margin + player.size)
      {
        if(position.x >= player.position.x && position.x <= player.position.x + player.size)
        {
          return true;
        }
        return false;
      }
    }
    else
    {
      for(int i = 0; i < 3; i++)
      {
        if(particles[i].collisionCheck())
        {
          return true;
        }
      }
      return false;
    }
    return false;
  }
}

class PowerCollection
{
  int rate;
  ArrayList<Power> powers;
  PowerCollection(int srate)
  {
    rate = srate;
    powers = new ArrayList<Power>();
  }
  void update()
  {
    if (int(random(0, rate)) == 0)
    {
      powers.add(new Power());
    }
    for(int i = 0; i < powers.size(); i++)
    {
      Power s = powers.get(i);
      s.update();
      if(s.position.y < margin)
      {
        powers.remove(i);
        i--;
      }
    }
  }
}

class Power
{
  PVector position;
  PVector velocity;
  PVector acceleration;
  
  Power()
  {
    acceleration = new PVector(0, (height/480)*-0.05);
    velocity = new PVector((width/640)*random(-1,1), (height/480)*random(0, 2));
    position = new PVector((width/640)*random(100, 500), (height/480)*480);
  }
  void update()
  {
    velocity.add(acceleration);
    position.add(velocity);
    
    c = color(0, 0, 255);  // Define color 'c'
    pg.fill(c);  // Use color variable 'c' as fill color
    pg.noStroke();  // Don't draw a stroke around shapes
    pg.ellipse(position.x, position.y, (height/480)*8, (height/480)*8);
  }
}
